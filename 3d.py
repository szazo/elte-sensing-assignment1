import time
import argparse
import open3d as o3d

# Parse the arguments
parser = argparse.ArgumentParser(
    prog = "PointCloud Poisson reconstruction",
    description = "Converts point cloud to 3d triangle mesh."
)
parser.add_argument("input", help="The input point cloud filepath (.xyz)")
parser.add_argument("output_prefix", help="The output prefix for images, clouds to be exported")
parser.add_argument("--gui", action=argparse.BooleanOptionalAction, default=True)
parser.add_argument("--depth", type=int,  default=12)

args = parser.parse_args()
input_filepath = args.input
output_prefix = args.output_prefix
show_gui = args.gui
depth = args.depth

# Visualization parameters
# render_params = dict(zoom=0.65,
#                      front=[-.22, 0, 1],
#                      up=[0, -1, 0],
#                      lookat=[94,  8, 1625])
render_params = dict(zoom=0.65,
                     front=[-.22, -0.1, 1],
                     up=[0, -1, 0],
                     lookat=[58.499428571428638, -5.3192023809523619, -2652.7550000000001])


def visualize(geometries, out_filepath=None, show_gui=True, point_show_normals=False, mesh_show_back_face=False,
              mesh_show_wireframe=False):
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.set_full_screen(True)

    for geometry in geometries:
        vis.add_geometry(geometry)

    # camera params
    ctr = vis.get_view_control()
    ctr.set_lookat(render_params['lookat'])
    ctr.set_up(render_params['up'])
    ctr.set_front(render_params['front'])
    ctr.set_zoom(render_params['zoom'])

    # render options
    opt = vis.get_render_option()
    opt.point_show_normal = point_show_normals
    opt.mesh_show_back_face = mesh_show_back_face
    opt.mesh_show_wireframe = mesh_show_wireframe

    # update
    vis.poll_events()
    vis.update_renderer()

    # img = vis.capture_screen_float_buffer(True)
    # plt.imshow(img)
    # plt.savefig('foo2.png')

    # capture
    if out_filepath is not None:
        vis.capture_screen_image(out_filepath)

    # show gui
    if show_gui:
        vis.run()

    # cleanup
    vis.destroy_window()

def select_inlier_outlier(cloud, index_mask):
    inlier_cloud = cloud.select_by_index(index_mask)
    outlier_cloud = cloud.select_by_index(index_mask, invert=True)

    return inlier_cloud, outlier_cloud

def display_inlier_outlier(inlier_cloud, outlier_cloud):

    outlier_cloud.paint_uniform_color([1, 0, 0]) # red
    inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8]) # gray

    visualize([outlier_cloud, inlier_cloud], show_gui=show_gui,
              out_filepath=f'{output_prefix}_pcd_inlier_outliers.png')


pcd = o3d.io.read_point_cloud(input_filepath)
print('Original cloud size: ', len(pcd.points))

# Downsample
voxel_size = 7.
downsampled_pcd = pcd.voxel_down_sample(voxel_size=voxel_size)
print(f"Cloud size after downsample (with voxel size {voxel_size}): ", len(downsampled_pcd.points))

# Outlier removal
filtered_pcd, index_mask = downsampled_pcd.remove_statistical_outlier(nb_neighbors=20,
                                                                      std_ratio=0.5)

inlier_cloud, outlier_cloud = select_inlier_outlier(downsampled_pcd, index_mask)
print('Point cloud size after outlier removal: ', len(filtered_pcd.points))

# Show inliers and outliers
display_inlier_outlier(inlier_cloud, outlier_cloud)

# Show filtered point cloud
visualize([filtered_pcd],
          show_gui=show_gui, out_filepath=f'{output_prefix}_pcd_filtered.png')

# Normal estimation
print('Estimating normals...')
filtered_pcd.estimate_normals()
filtered_pcd.orient_normals_consistent_tangent_plane(100)

o3d.io.write_point_cloud('output_normals.xyz', filtered_pcd, write_ascii=True)

# Show normals
visualize([filtered_pcd],
          show_gui=show_gui, out_filepath=f'{output_prefix}_pcd_normals.png',
          point_show_normals=True)

# Poisson surface reconstruction
print('Poisson reconstruction...')
poisson_mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(filtered_pcd,
                                                                                    depth=depth,
                                                                                    scale=1.0,
                                                                                    linear_fit=False)

# Show reconstructed surface
poisson_mesh.paint_uniform_color([1, 0.706, 0])
poisson_mesh.compute_vertex_normals()
visualize([poisson_mesh],
          show_gui=show_gui, out_filepath=f'{output_prefix}_triangle_mesh.png',
          mesh_show_back_face=True,
          mesh_show_wireframe=True)

# Write the surface mesh as ply
o3d.io.write_triangle_mesh(f'{output_prefix}.ply', poisson_mesh)
