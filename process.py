import os
import itertools
import subprocess
import pandas as pd

input_dirs = ['Art', 'Books', 'Dolls', 'Laundry', 'Moebius', 'Reindeer']
input_dmins = [200, 200, 200, 230, 200, 230]
surface_reconstruction_depth = 12

baseline_mm = 160
focal_length_px = 3740

out_dir = 'output'

os.makedirs(out_dir, exist_ok=True)
inputs = [[
    d,
    f'data/{d}/view0.png',
    f'data/{d}/view1.png',
    f'data/{d}/disp1.png',
    f'{out_dir}/{d}'] for d in input_dirs]

# append dmins
list(map(list.append, inputs, input_dmins))

#  hyperparameter combinations
hyperparams = {
    'naive': {
        'window_size': [3, 5, 7, 9],
        # 'lambda': [1.0, 2.0, 4.0, 5.0, 10.0]
    },
    'dp': {
        'window_size': [1, 3, 5, 7],
        'lambda': [10.0, 20.0, 30.0]
    },
    'opencv': {
        'window_size': [5, 7, 9, 11, 15],
    }
}

# these combinations will be used for 3d reconstruction (algo,window_size,lambda)
threed_combinations = [ ('naive', 9, 0.),
                        ('dp', 1, 30.),
                        ('opencv', 9, 0.) ]


# this will contain the result
experiments_df = pd.DataFrame()

for name, image1, image2, ground_truth, output, dmin in inputs:
    print(name, image1, image2, ground_truth, output, dmin)

    for algo, params in hyperparams.items():

        window_sizes = params['window_size']
        lambdas = params['lambda'] if 'lambda' in params else [0]

        param_product = list(itertools.product(window_sizes, lambdas))

        for window_size, lambd in param_product:
            print(f'processing "{name}" with algorithm "{algo}" (window_size={window_size}, lambda={lambd})')

            suffix = f'_{algo}_{window_size}_{lambd}'
            output_with_suffix = output + suffix

            # execute the algorithm
            process = subprocess.run(['build/assignment1_stereo',
                                      algo,
                                      '--focallength', str(focal_length_px),
                                      '--baseline', str(baseline_mm),
                                      '--windowsize', str(window_size),
                                      '--lambda', str(lambd),
                                      '--dmin', str(dmin),
                                      '--nogui',
                                      '-g',
                                      ground_truth,
                                      image1,
                                      image2,
                                      output_with_suffix],
                                     universal_newlines=True)

            if process.returncode != 0:
                exit(1)

            # 3d normal estimation and surface reconstruction
            has_3d_reconstruction = False
            match = [x for x in threed_combinations if x[0] == algo and x[1] == window_size and x[2] == lambd]
            if len(match) > 0:
                process = subprocess.run(['python3',
                                          '3d.py',
                                          '--depth', str(surface_reconstruction_depth),
                                          '--no-gui',
                                          f'{output_with_suffix}.xyz',
                                          output_with_suffix],
                                         universal_newlines=True)
                has_3d_reconstruction = True
                if process.returncode != 0:
                    exit(1)

            # load the stat
            df = pd.read_csv(output_with_suffix + "_stat.csv")
            df.insert(0, 'experiment', name, True)
            df.insert(1, 'algorithm', algo, True)
            df.insert(2, 'window_size', window_size)
            df.insert(3, 'lambda', lambd)
            df.insert(4, 'has_3d', has_3d_reconstruction)

            # add to the list
            experiments_df = pd.concat([experiments_df, df])
            experiments_df.to_csv(f'{out_dir}/experiments.csv', index=False)
