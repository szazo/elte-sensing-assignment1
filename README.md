# Disparity Map Estimation and Stereo Reconstruction

Disparity Map estimation and Stereo Reconstruction using several disparity estimation algorithms.

More information and results can be found here: https://colab.research.google.com/drive/13tlWp-Z9OCiXknd2aWU7U-OAnhy6RfVl?usp=sharing

## Getting started

To build and run the experiments, use the following commands

Build:
```
mkdir build
cd build
cmake ..
make
cd ..
```

To run the experiments:

```
python3 process.py
```

## Dataset

Part of the following dataset was used for the experiments: https://vision.middlebury.edu/stereo/data/scenes2005/
