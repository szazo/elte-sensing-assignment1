#pragma once

#include <opencv2/core/mat.hpp>

void StereoEstimation_Naive(const int &window_size, const int &dmin,
                            const int &height, const int &width,
                            const cv::Mat &image1, const cv::Mat &image2,
                            cv::Mat &naive_disparities);

void StereoEstimation_DP(const int &window_size, const float &lambda,
                         const int &dmin, const int &height, const int &width,
                         const cv::Mat &image1, const cv::Mat &image2,
                         cv::Mat &dp_disparities);

void StereoEstimation_OpenCV(const int &window_size, const cv::Mat &image1,
                             const cv::Mat &image2, cv::Mat &disparities);

void Disparity2PointCloud(const std::string &output_file, const int &height,
                          const int &width, const cv::Mat &disparities,
                          const int &window_size, const int &dmin,
                          const double &baseline, const double &focal_length);

void evaluate(const cv::Mat &disparities_est, const cv::Mat &disparities_gt,
              cv::Mat &difference_image, std::string &stat_filename,
              const double &elapsed_time);

// https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
inline std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch (depth) {
  case CV_8U:
    r = "8U";
    break;
  case CV_8S:
    r = "8S";
    break;
  case CV_16U:
    r = "16U";
    break;
  case CV_16S:
    r = "16S";
    break;
  case CV_32S:
    r = "32S";
    break;
  case CV_32F:
    r = "32F";
    break;
  case CV_64F:
    r = "64F";
    break;
  default:
    r = "User";
    break;
  }

  r += "C";
  r += (chans + '0');

  return r;
}
