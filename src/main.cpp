#include "main.h"
#include "cxxopts.hpp"
#include <cmath>
#include <complex>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/quality.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>
#include <ostream>
#include <sstream>
#include <string>

const int DP_M_MATCH = 1;
const int DP_M_LEFT_OCCLUSION = 2;
const int DP_M_RIGHT_OCCLUSION = 3;

const int MAX_DISPARITY = 255;

enum class Method { naive, dp, opencv };

int main(int argc, char **argv) {

  // parse commandline arguments
  cxxopts::Options options("Stereo", "Sensing Assignment 1 for Stereo Vision");
  options.add_options()(
      "method", "Method of estimation; possible values: 'naive','dp','opencv'",
      cxxopts::value<std::string>())("image1", "The left image",
                                     cxxopts::value<std::string>())(
      "image2", "The right image", cxxopts::value<std::string>())(
      "output", "The name of the output", cxxopts::value<std::string>())(
      "g,groundtruth", "The ground truth image for the comparison",
      cxxopts::value<std::string>())(
      "f,focallength", "The focal length in pixels",
      cxxopts::value<double>()->default_value("3740"))(
      "b,baseline", "The baseline in mm",
      cxxopts::value<double>()->default_value("160"))(
      "d,dmin",
      "To map the disparities into 3D coordinates, add this value to each "
      "disparity value.",
      cxxopts::value<int>()->default_value("200"))(
      "w,windowsize",
      "The window size used in all of the disparity algorithms. It should be "
      "odd.",
      cxxopts::value<int>()->default_value("3"))(
      "l,lambda", "The lambda parameter for the dynamic programming approach",
      cxxopts::value<float>())("n,nogui", "Do not show result in GUI window")(
      "h,help", "Print usage");

  options.positional_help("<naive|dp|opencv>> <image1> <image2> <output>");
  options.parse_positional({"method", "image1", "image2", "output"});

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help() << std::endl;
    exit(0);
  }

  if (!result.count("method") || !result.count("image1") ||
      !result.count("image2") || !result.count("output")) {
    std::cout << options.help() << std::endl;
    exit(1);
  }

  // camera setup parameters
  const std::string method_string = result["method"].as<std::string>();
  const double focal_length = result["focallength"].as<double>(); // pixels
  const double baseline = result["baseline"].as<double>();        // mm

  // stereo estimation parameters
  const int dmin = result["dmin"].as<int>();
  const int window_size = result["windowsize"].as<int>();

  const bool no_gui = result["nogui"].as<bool>();

  if (window_size % 2 == 0) {
    std::cerr << "Window size should be odd: " << window_size << std::endl;
    return EXIT_FAILURE;
  }

  // parse the method
  Method method;
  if (method_string == "naive") {
    method = Method::naive;
  } else if (method_string == "dp") {
    method = Method::dp;
  } else if (method_string == "opencv") {
    method = Method::opencv;
  } else {
    std::cerr << "Invalid method (possible values: naive,dp,opencv): "
              << method_string << std::endl;
    return EXIT_FAILURE;
  }

  // read the input images
  cv::Mat image1 =
      cv::imread(result["image1"].as<std::string>(),
                 cv::IMREAD_GRAYSCALE); // pixel fromat will be uchar ... 8bit
  cv::Mat image2 =
      cv::imread(result["image2"].as<std::string>(), cv::IMREAD_GRAYSCALE);
  const std::string output_file = result["output"].as<std::string>();

  if (!image1.data) {
    std::cerr << "No image1 data" << std::endl;
    return EXIT_FAILURE;
  }

  if (!image2.data) {
    std::cerr << "No image2 data" << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "------------------ Parameters -------------------" << std::endl;
  std::cout << "method = " << method_string << std::endl;
  std::cout << "focal_length = " << focal_length << std::endl;
  std::cout << "baseline = " << baseline << std::endl;
  std::cout << "window_size = " << window_size << std::endl;
  std::cout << "disparity added due to image cropping = " << dmin << std::endl;
  std::cout << "output filename = " << output_file << std::endl;
  std::cout << "-------------------------------------------------" << std::endl;

  int height = image1.size().height;
  int width = image1.size().width;

  // reconstruction using the selected method
  cv::Mat disparities = cv::Mat::zeros(height, width, CV_8UC1); // 8bit, uchar

  int64 start_time = cv::getTickCount();

  switch (method) {

  case Method::naive: {
    StereoEstimation_Naive(window_size, dmin, height, width, image1, image2,
                           disparities);

    break;
  }
  case Method::dp: {

    if (result.count("lambda") == 0) {
      std::cerr << "ERROR: Missing 'lambda' parameter" << std::endl;
      exit(1);
    }

    const float lambda = result["lambda"].as<float>();
    StereoEstimation_DP(window_size, lambda, dmin, height, width, image1,
                        image2, disparities);

    break;
  }
  case Method::opencv: {
    StereoEstimation_OpenCV(window_size, image1, image2, disparities);

    break;
  }
  }

  double elapsed_time =
      ((double)(cv::getTickCount() - start_time)) / cv::getTickFrequency();
  std::cout << "Elapsed time: " << elapsed_time << std::endl;

  // if we have ground truth file, do the evaluation
  cv::Mat ground_truth;
  if (result.count("groundtruth") > 0) {
    std::string ground_truth_filename = result["groundtruth"].as<std::string>();
    std::cout << "Evaluating using ground truth image " << ground_truth_filename
              << std::endl;
    ground_truth = cv::imread(ground_truth_filename, cv::IMREAD_GRAYSCALE);

    cv::Mat difference_image;
    std::string stat_filename = output_file + "_stat.csv";
    evaluate(disparities, ground_truth, difference_image, stat_filename,
             elapsed_time);

    cv::imwrite(output_file + "_difference.png", difference_image);
  }

  // 3D reconstruction
  Disparity2PointCloud(output_file, height, width, disparities, window_size,
                       dmin, baseline, focal_length);

  // save / display images
  std::stringstream out1;
  out1 << output_file << "_output.png";
  cv::imwrite(out1.str(), disparities);

  if (!no_gui) {
    // show the results
    cv::namedWindow("Naive", cv::WINDOW_AUTOSIZE);
    cv::imshow("Naive", disparities);
    std::cout << "result: " << disparities.cols << " " << disparities.rows
              << " " << type2str(disparities.type()) << std::endl;

    cv::waitKey(0);
  }

  return 0;
}

// evaluate the disparity map using ground truth
void evaluate(const cv::Mat &disparities_est, const cv::Mat &disparities_gt,
              cv::Mat &difference_image, std::string &stat_filename,
              const double &elapsed_time) {

  if (disparities_est.rows != disparities_gt.rows ||
      disparities_est.cols != disparities_gt.cols) {
    std::cerr << "ERROR: Ground truth disparity image size mismatch, exiting: "
              << disparities_gt.rows << ", " << disparities_gt.cols
              << std::endl;
    exit(1);
  }

  // create the difference image
  cv::absdiff(disparities_est, disparities_gt, difference_image);

  // several metrics
  cv::Mat squared_difference;
  cv::pow(difference_image, 2, squared_difference);

  const double n = disparities_est.rows * disparities_est.cols;
  const double ssd = cv::sum(squared_difference)[0];
  const double mse = ssd / n;
  const double rmse = std::sqrt(mse);

  const double sad = cv::sum(difference_image)[0];
  const double mad = sad / n;

  // SSIM
  cv::Mat ssid_quality_map;
  double ssim = cv::quality::QualitySSIM::compute(
      disparities_gt, disparities_est, ssid_quality_map)[0];

  // PSNR
  cv::Mat psnr_quality_map;
  double psnr = cv::quality::QualityPSNR::compute(
      disparities_gt, disparities_est, psnr_quality_map)[0];

  // write into the stat file
  std::ofstream stat_file;
  stat_file.open(stat_filename);

  stat_file << "ssd,mse,rmse,sad,mad,ssim,psnr,time\n";
  stat_file << ssd << "," << mse << "," << rmse << "," << sad << "," << mad
            << "," << ssim << "," << psnr << "," << elapsed_time << "\n";
  stat_file.close();
}

// get the final disparity value clamped to 8bit
inline uchar clamp_disparity(int disparity) {
  disparity = std::abs(disparity);

  if (disparity > MAX_DISPARITY) {
    disparity = 0;
  }

  uchar disparity_to_store = static_cast<uchar>(disparity);
  return disparity_to_store;
}

// stereo estimation using OpenCV's StereoSGBM method
void StereoEstimation_OpenCV(const int &window_size, const cv::Mat &image1,
                             const cv::Mat &image2, cv::Mat &disparities) {

  cv::Ptr<cv::StereoSGBM> sgbm =
      cv::StereoSGBM::create(0, MAX_DISPARITY, window_size);

  cv::Mat opencv_disparities;
  sgbm->compute(image1, image2, opencv_disparities);
  cv::ximgproc::getDisparityVis(opencv_disparities, disparities);
}

// stereo estimation using naive optimization
void StereoEstimation_Naive(const int &window_size, const int &dmin,
                            const int &height, const int &width,
                            const cv::Mat &image1, const cv::Mat &image2,
                            cv::Mat &naive_disparities) {
  int half_window_size = window_size / 2;
  int progress = 0;

#pragma omp parallel for
  for (int i = half_window_size; i < height - half_window_size; ++i) {

#pragma omp critical
    {
      ++progress;

      std::cout << "Calculating disparities for the naive approach... "
                << std::ceil(((progress) /
                              static_cast<double>(height - window_size + 1)) *
                             100)
                << "%\r" << std::flush;
    }

#pragma omp parallel for
    for (int j = half_window_size; j < width - half_window_size; ++j) {

      // (i,j) -> (row, col)

      int min_ssd = INT_MAX;
      int disparity = 0;

      // for each (i,j) do a 1D search for the best disparity
      for (int d = -j + half_window_size; d < width - half_window_size - j;
           ++d) {
        int ssd = 0;

        for (int u = j - half_window_size; u <= j + half_window_size; ++u) {
          for (int v = i - half_window_size; v <= i + half_window_size; ++v) {

            assert(u >= 0 && "u out of left boundary");
            assert(u + d < width && "u out of right boundary");
            assert(v >= 0 && "v out of top boundary");
            assert(v < height && "v out of bottom boundary");

            uchar pixel_left = image1.at<uchar>(v, u);
            uchar pixel_right = image2.at<uchar>(v, u + d);

            ssd += std::pow(pixel_left - pixel_right, 2);
          }
        }

        if (ssd < min_ssd) {
          min_ssd = ssd;
          disparity = d;
        }
      }

      naive_disparities.at<uchar>(i, j) = clamp_disparity(disparity);
    }
  }

  std::cout << "Calculating disparities for the naive approach... Done.\r"
            << std::flush;
  std::cout << std::endl;
}

// stereo estimation using dynamic programming approach
void StereoEstimation_DP(const int &window_size, const float &lambda,
                         const int &dmin, const int &height, const int &width,
                         const cv::Mat &image1, const cv::Mat &image2,
                         cv::Mat &dp_disparities) {

  int half_window_size = window_size / 2;
  int progress = 0;

// for each row (scanline)
#pragma omp parallel for
  for (int y_0 = half_window_size; y_0 < height - half_window_size; ++y_0) {

#pragma omp critical
    {
      ++progress;

      std::cout << "Calculating disparities for the DP approach... "
                << std::ceil(((progress) /
                              static_cast<double>(height - window_size + 1)) *
                             100)
                << "%\r" << std::flush;
    }

    // creating dissimilarity image
    cv::Mat dissim = cv::Mat::zeros(width, width, CV_32FC1);

    int range_min = half_window_size;
    int range_max = width - half_window_size - 1;

#pragma omp parallel for
    for (int i = range_min; i <= range_max; ++i) { // left image

#pragma omp parallel for
      for (int j = range_min; j <= range_max; ++j) { // right image

        float sum = 0;
        for (int u = -half_window_size; u <= half_window_size; ++u) {
          for (int v = -half_window_size; v <= half_window_size; ++v) {

            float i1 = static_cast<float>(image1.at<uchar>(y_0 + v, i + u));
            float i2 = static_cast<float>(image2.at<uchar>(y_0 + v, j + u));

            sum += std::pow(i1 - i2, 2); // SSD (sum of square differences)
          }
        }

        const float mse = sum / (window_size * window_size); // mean

        dissim.at<float>(i, j) = mse;
      }
    }

    // building the C and M tables
    cv::Mat C = cv::Mat::zeros(width, width, CV_32FC1);
    cv::Mat M = cv::Mat::zeros(width, width, CV_8UC1);

    // initialize the first row and column
    for (int i = range_min; i <= range_max; ++i) {
      // first row
      C.at<float>(range_min, i) = dissim.at<float>(range_min, i);
      M.at<uchar>(range_min, i) = DP_M_RIGHT_OCCLUSION;

      // first column
      C.at<float>(i, range_min) = dissim.at<float>(i, range_min);
      M.at<uchar>(i, range_min) = DP_M_LEFT_OCCLUSION;
    }

    // build up the M,C tables
    for (int i = range_min + 1; i <= range_max; ++i) {
      for (int j = range_min + 1; j <= range_max; ++j) {

        // calculate the minimum
        float match = C.at<float>(i - 1, j - 1) + dissim.at<float>(i, j);
        float left_occl = C.at<float>(i - 1, j) + lambda;
        float right_occl = C.at<float>(i, j - 1) + lambda;

        float c = match;
        uchar m = DP_M_MATCH;

        if (left_occl < c) {
          m = DP_M_LEFT_OCCLUSION;
          c = left_occl;
        }

        if (right_occl < c) {
          m = DP_M_RIGHT_OCCLUSION;
          c = right_occl;
        }

        C.at<float>(i, j) = c;
        M.at<uchar>(i, j) = m;
      }
    }

    // trace back from sink to source
    int i = range_max;
    int j = range_max;

    while (i > range_min || j > range_min) {

      uint m = M.at<uchar>(i, j);
      switch (m) {
      case DP_M_MATCH: {

        // it is a match, write to the output
        int disparity = j - i;
        dp_disparities.at<uchar>(y_0, j) =
            clamp_disparity(disparity); //  disparity < 255 ? disparity : 0;

        i--;
        j--;

        break;
      }
      case DP_M_LEFT_OCCLUSION: {
        i--;
        break;
      }
      case DP_M_RIGHT_OCCLUSION: {
        j--;
        break;
      }
      }
    }
  }

  std::cout << "Calculating disparities for the DP approach... Done.\r"
            << std::flush;
  std::cout << std::endl;
}

// create point cloud from disparity map
void Disparity2PointCloud(const std::string &output_file, const int &height,
                          const int &width, const cv::Mat &disparities,
                          const int &window_size, const int &dmin,
                          const double &baseline, const double &focal_length) {
  const auto &b = baseline;
  const auto &f = focal_length;

  std::stringstream out3d;
  out3d << output_file << ".xyz";
  std::ofstream outfile(out3d.str());
  for (int i = 0; i < height - window_size; ++i) {
    std::cout << "Reconstructing 3D point cloud from disparities... "
              << std::ceil(
                     ((i) / static_cast<double>(height - window_size + 1)) *
                     100)
              << "%\r" << std::flush;
    for (int j = 0; j < width - window_size; ++j) {
      if (disparities.at<uchar>(i, j) == 0)
        continue;

      const double d = static_cast<double>(disparities.at<uchar>(i, j)) + dmin;

      const double u1 = j - (width / 2.0);
      const double u2 = u1 - d;

      const double v = i - (height / 2.0);

      double Z = (baseline * focal_length) / d; // mm * px / px
      Z = -Z; // to be same orientation as the others
      const double X = -(baseline * (u1 + u2)) / (2 * d); // mm * px / px
      const double Y = (baseline * v) / d;                // mm * px / px

      outfile << X << " " << Y << " " << Z << std::endl;
    }
  }

  std::cout << "Reconstructing 3D point cloud from disparities... Done.\r"
            << std::flush;
  std::cout << std::endl;
}
